# Zephr NodeJS SDK

## Overview
Blaize uses web APIs for all its functionality. These are split into two categories: the Admin API and the Public API. All functionality of the system can be controlled by the Admin API. The Public API is used for client-side implementations and is tightly linked to a user’s session.

You can find out more about the design principals of the APIs on our wiki (you may need to request access). You can read the Admin API and Public API specifications online.

The Blaize SDK is a wrapper around the Blaize APIs allowing for rapid integration development.

## Supported languages
The Blaize SDK is supplied in the following programming languages:

* Java (recommended for Android development)
* JavaScript
* Swift (recommended for iOS development)
* PHP

## Getting the SDK
The Blaize JavaScript SDK is available as an NPM module. Contact support@blaize.io to arrange access.

    const ZephrSDK = require('@zephr/node-sdk');
    const PublicApiClient = ZephrSDK.PublicApiClient;
    const AdminApiClient = ZephrSDK.AdminApiClient;


## Client-side SDK
The client-side SDK requires you to create an instance of the PublicApiClient, providing the tenantId.

    let client = PublicApiClient.build(tenant);

Optionally, Blaize environment can be provided, too:

    let client = PublicApiClient.build(tenant, 'staging');

It is possible to specify a base URL for the Blaize service, if it is non-standard (not normally needed):

    let client = PublicApiClient.build(tenant).overrideBaseUrl('http://localhost:8080');

For client-side implementations the SDK supports the following function

Register:

    const PublicApiClient = require('@zephr/node-sdk').PublicApiClient;
    
    let tenant = 'mysite';
    
    let client = PublicApiClient.build(tenant);
    
    let sessionId = await client.register({
        identifiers: {
            'email_address': 'helen@blaize.io'
        },
        validators: {
            'password': 'sup3r_s3cr3t'
        },
        attributes: {
            'foo': 'bar'
        }
    });

Login:

    const PublicApiClient = require('@zephr/node-sdk').PublicApiClient;
    
    let tenant = 'mysite';
    
    let client = PublicApiClient.build(tenant);
    
    let sessionId = await client.login({
        identifiers: {
            'email_address': 'helen@blaize.io'
        },
        validators: {
            'password': 'sup3r_s3cr3t'
        }
    });

Logout:

    const PublicApiClient = require('@zephr/node-sdk').PublicApiClient;
    
    let tenant = 'mysite';

    let client = PublicApiClient.build(tenant);
    
    let sessionId = getSessionId()   // you need to implement this

    client.logout(sessionId);

Forget me:

    const PublicApiClient = require('@zephr/node-sdk').PublicApiClient;
    
    let tenant = 'mysite';
    
    let client = PublicApiClient.build(tenant);

    let sessionId = getSessionId()   // you need to implement this

    client.forgetMe(sessionId);

Entitlement challenge:

    const PublicApiClient = require('@zephr/node-sdk').PublicApiClient;
    
    let tenant = 'mysite';
    let articleEntitlementId = '5341dc17-f91b-4311-b9ee-6906024c87a2';
    let videoEntitlementId = '2b7fa1f5-795d-459e-84eb-8e0c62fb018f';
    
    let client = PublicApiClient.build(tenant);
    
    let sessionId = getSessionId()   // you need to implement this
    
    let results = await client.challenge(sessionId, [articleEntitlementId, videoEntitlementId]);
    
    if (! results[articleEntitlementId]) {
        console.log("You cannot view articles");
    } if (! results[videoEntitlementId ]) {
        console.log("You cannot view videos");
    }

Request-rule decision:

    const PublicApiClient = require('@zephr/node-sdk').PublicApiClient;

    let tenant = 'mysite';
    
    let client = PublicApiClient.build(tenant);
    
    let sessionId = getSessionId()   // you need to implement this
    
    let decision = await client.accessDecision(sessionId, '/some/content', 'GET', {referrer: 'https://www.facebook.com'});
    
    console.log(decision.status);
    console.log(decision.body);

Get profile:

    const PublicApiClient = require('@zephr/node-sdk').PublicApiClient;
    
    let tenant = 'mysite';
    
    let client = PublicApiClient.build(tenant);
    
    let sessionId = getSessionId()   // you need to implement this
    
    let profile = await client.retrieveProfile(sessionId);
    
    console.log(JSON.stringify(profile));

Update profile:

    const PublicApiClient = require('@zephr/node-sdk').PublicApiClient;

    let tenant = 'mysite';
    
    let client = PublicApiClient.build(tenant);
    
    let profile = {
        'favourite-color': 'Red'
    };
    let merge = true; // if false, the new profile will completely replace the entire old profile, if true only provided fields will be replaced
    
    let sessionId = getSessionId()   // you need to implement this
    
    await client.updateProfile(sessionId, profile, merge);

Get extended profile document:

    const PublicApiClient = require('@zephr/node-sdk').PublicApiClient;
    
    let tenant = 'mysite';
    
    let client = PublicApiClient.build(tenant);
    
    let sessionId = getSessionId()   // you need to implement this
    
    let profile = await client.retrieveExtendedProfile(sessionId, 'reading-history');
    
    console.log(JSON.stringify(profile));

Update extended profile document:

    const PublicApiClient = require('@zephr/node-sdk').PublicApiClient;

    let tenant = 'mysite';
    
    let client = PublicApiClient.build(tenant);
    
    let profile = {
        'recent': ['/1.html', '/2.html']
    };
    
    let sessionId = getSessionId()   // you need to implement this
    
    await client.writeExtendedProfile(sessionId, 'reading-history', profile);

## Server-side SDK

The server-side SDK is essentially a REST client which performs HMAC request signing for the API. This SDK requires the implementor to specify the host, path, method, body, headers and query parameters for a request and returns the response as a JSON object or plain text.

In order to make requests to the Admin API you will need to first create a keypair, which you can do in the Blaize Admin Console.

One can also use the server-side SDK to sign request for use with a different HTTP client.

Example REST request

    const AdminApiClient = require('@zephr/node-sdk').AdminApiClient;
    
    let tenant = 'mysite';
    
    const accessKey = getAccessKeySecurely();   // you need to implement this
    const secretKey = getSecretKeySecurely();   // you need to implement this

    let client = AdminApiClient.build(accessKey, secretKey, tenant);
    
    let responseBody = await client.get('/v3/users');

Example request using a 3rd party HTTP client

    const AdminApiClient = require('@zephr/node-sdk').AdminApiClient;
    const axios = require('axios');
    
    let tenant = 'mysite';
    
    const accessKey = getAccessKeySecurely();   // you need to implement this
    const secretKey = getSecretKeySecurely();   // you need to implement this

    let body = ''; // This needs to be bytewise identical to the payload, whitespace included
    let path = '/v3/users';
    let method = 'GET';
    let timestamp = new Date().getTime();
    let nonce = Math.round(Math.random() * 1000000000000).toString(16);

    let authorizationHeader = AdminApiClient.HmacSigner.buildAuthHeader(accessKey, secretKey, path, method, body)
    
    axios.get('https://admin.' + tenant + '.blaize.io' + path, {
        headers: {
            authorization: authorizationHeader 
        }
    });
