const axios = require('axios')
const BLAIZE_SESSION_COOKIE_PATTERN = 'blaize_session=([a-zA-Z0-9\\-_]+)([ ;].*)?'

class PublicApiClient {

  constructor(tenant, environment) {
    this.url = `https://${tenant}.cdn${(!!environment ? `.${environment}.` : '.')}blaize.io`
  }

  overrideBaseUrl(url) {
    this.url = url
    return this
  }

  register(userRegistrationPayload) {

    const headers = {
      'Content-Type': 'application/json',
    }

    return httpRequest(this.url, '/blaize/register', 'POST', headers, userRegistrationPayload, true)
      .then(({ headers }) => extractSessionId(headers))
  }

  login(userLoginPayload) {

    const headers = {
      'Content-Type': 'application/json',
    }

    return httpRequest(this.url, '/blaize/login', 'POST', headers, userLoginPayload, true)
      .then((response) => extractSessionId(response.headers))
  }

  logout(sessionId) {

    const headers = {
      'Content-Type': 'application/json',
      'Cookie': `blaize_session=${sessionId}`,
    }

    return httpRequest(this.url, '/blaize/logout', 'POST', headers)
  }

  forgetMe(sessionId) {

    const headers = {
      'Content-Type': 'application/json',
      'Cookie': `blaize_session=${sessionId}`,
    }

    return httpRequest(this.url, '/blaize/forget-me', 'POST', headers)
  }

  getUserProfile(sessionId) {

    const headers = {
      'Content-Type': 'application/json',
      'Cookie': `blaize_session=${sessionId}`,
    }

    return httpRequest(this.url, '/blaize/profile', 'GET', headers)
      .then(({ data }) => data)
  }

  updateUserProfile(sessionId, profile, merge) {

    if (profile instanceof Object) {
      throw new Error('profile needs to be a JSON object')
    }

    const headers = {
      'Content-Type': 'application/json',
      'Cookie': `blaize_session=${sessionId}`,
    }

    return httpRequest(this.url, '/blaize/profile', Boolean(merge) ? 'PATCH' : 'PUT', headers)
  }

  challenge(sessionId, entitlements) {

    if (entitlements instanceof Array) {
      throw new Error('entitlements need to be an array of entitlement ids')
    }

    const headers = {
      'Content-Type': 'application/json',
      'Cookie': `blaize_session=${sessionId}`,
    }

    const payload = {
      entitlementIds: entitlements,
    }

    return httpRequest(this.url, '/blaize/authorization/challenge', 'POST', headers, payload)
      .then(({ data }) => data)
  }

  accessDecision(sessionId, requestPath, requestMethod, requestHeaders) {

    const headers = {
      'Content-Type': 'application/json',
      'Cookie': `blaize_session=${sessionId}`,
      ...requestHeaders,
    }

    return httpRequest(this.url, `/blaize/decision-engine?path=${requestPath}&http_method=${requestMethod}`, 'GET', headers)
      .then(({ data, status }) => ({
        body: data,
        status,
      }))
  }

  featureAccessDecision(requestHeaders, requestBody) {

    const headers = {
      'Content-Type': 'application/json',
      ...requestHeaders,
    }

    if(typeof requestBody.sdkFeatureSlug !== 'string') {
      throw new Error('sdkFeatureSlug body property of type string is required');
    }

    return httpRequest(
      this.url,
      '/zephr/decision-engine',
      'POST',
      headers,
      requestBody
    ).then(({ data, status }) => ({
      body: data,
      status,
    }));
  }

  getUserExtendedProfile(sessionId, profileDocumentId) {

    const headers = {
      'Cookie': `blaize_session=${sessionId}`,
    }

    return httpRequest(this.url, `/blaize/profile/${profileDocumentId}`, 'GET', headers)
      .then(({ data }) => data)
  }

  updateUserExtendedProfile(sessionId, profileDocumentId, profileDocument) {

    const headers = {
      'Content-Type': 'application/json',
      'Cookie': `blaize_session=${sessionId}`,
    }

    return httpRequest(this.url, `/blaize/profile/${profileDocumentId}`, 'PUT', headers, profileDocument)
  }
}

function extractSessionId(headers) {
  const setCookieHeaders = headers['set-cookie']
  for (const cookieHeader of setCookieHeaders) {
    for (const value of cookieHeader.split(';')) {
      const matchedGroup = value.match(BLAIZE_SESSION_COOKIE_PATTERN)
      if (matchedGroup) {
        return matchedGroup[1]
      }
    }
  }
}

function httpRequest(host, path, method, headers, data, withCredential) {

  const url = host + path
  return axios({ url, method, headers, data, withCredential })
    .catch((error) => {
      console.error(error.response)
      return error
    })
}

function build(tenant, environment) {
  return new PublicApiClient(tenant, environment)
}

module.exports = {
  build,
}